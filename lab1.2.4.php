<?php

//==================== print rout to the script's directory
echo 'Rout to the script\'s directory: ' . __DIR__, PHP_EOL;

//==================== print script file name
echo 'Current script file name: ' . $_SERVER['SCRIPT_FILENAME'], PHP_EOL;

//==================== print number of currently executing string
echo 'Number of currently executing string: ' . __LINE__, PHP_EOL;

//==================== create a few of custom constants with checking to already existed constants
defined("COLOR_RED") || define("COLOR_RED", "Red");
defined("PI") || define("PI", "3.14159265359");

echo 'Defined color is ' . COLOR_RED, PHP_EOL;
echo 'Value of number PI commonly approximated as' . PI;