<?php

//$weekDay = date('w');
$weekDay = 4;


if ($weekDay == 5 || $weekDay == 6) {
    //weekend
    echo 'It\'s weekend!';
} else {
    //working day
    $currentHour = date('H');
    $startWorking = 9;
    $endWorking = 18;

    if ($currentHour >= $startWorking && $currentHour <= $endWorking) {
        //Middle of the day
        $hoursLeft = $endWorking - $currentHour;
        echo 'It\'s ' . $hoursLeft . ' hours left to work.';
    } elseif ($currentHour < $startWorking) {
        $hoursBeforeWork = $startWorking - $currentHour;
        echo 'After ' . $hoursBeforeWork . ' hours we\'ll start to work.';
    } elseif ($currentHour > $endWorking) {
        $hoursAfterWork = $currentHour - $endWorking;
        echo 'Workday ended ' . $hoursAfterWork . ' hour\'s ago.';
    } else {
        echo 'Wrong time!';
    }
}